# Create Certificates

```sh
# Certificate and key for gibz-app.local
openssl req -x509 -newkey rsa:4096 -keyout auth.gibz-app.key -out auth.gibz-app.crt -sha256 -days 365 -nodes -subj "/C=CH/ST=Zug/L=Zug/O=GIBZ/OU=GIBZ App/CN=auth.gibz-app.local"

```